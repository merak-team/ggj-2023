using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

namespace Roots
{
    public class LocaleSelector : MonoBehaviour
    {
        [SerializeField] GameObject[] localizedTitles;

        private void Start()
        {
            for (int i = 0; i < LocalizationSettings.AvailableLocales.Locales.Count; i++)
            {
                if (LocalizationSettings.SelectedLocale ==
                    LocalizationSettings.AvailableLocales.Locales[i])
                {
                    UpdateTitle(i);
                    break;
                }
            }
        }

        public void SelectLocale(int id)
        {
            LocalizationSettings.SelectedLocale =
                LocalizationSettings.AvailableLocales.Locales[id];
            UpdateTitle(id);
        }

        private void UpdateTitle(int id)
        {
            localizedTitles[id].SetActive(true);
            localizedTitles[1 - id].SetActive(false);
        }
    }
}