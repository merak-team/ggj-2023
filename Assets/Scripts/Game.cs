using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Roots
{
    public class Game : MonoBehaviour
    {
        /// <summary>
        /// 是否看过教程
        /// </summary>
        public static bool TipShowed { get; set; } = false;

        public bool Started { get; private set; } = false;
        public bool Paused { get; private set; } = false;

        public void StartGame() => Started = true;

        /// <summary>
        /// 重新读取场景
        /// </summary>
        public void Restart()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(gameObject.scene.buildIndex);
        }

        /// <summary>
        /// 退出游戏
        /// </summary>
        public void Quit()
        {
            Application.Quit();
        }

        /// <summary>
        /// 切换暂停/继续
        /// </summary>
        public void TogglePause()
        {
            Paused = !Paused;
            Time.timeScale = Paused ? 0f : 1f;
        }

        public void GameOver()
        {
            Started = false;
        }
    }
}