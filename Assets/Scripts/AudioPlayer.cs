using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hotran.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPlayer : MonoBehaviour
    {
        public static AudioPlayer Instance { get; private set; }

        private AudioSource audioSource;

        [SerializeField] AudioClip[] bgms;

        private void Awake()
        {
            Instance = this;
            audioSource = GetComponent<AudioSource>();
        }

        public void PlaySound(AudioClip audioClip) =>
            audioSource.PlayOneShot(audioClip);

        public void PlayRandom(params AudioClip[] audioClips)
        {
            int index = Random.Range(0, audioClips.Length);
            PlaySound(audioClips[index]);
        }

        public void SwitchBGM(int id)
        {
            if (audioSource.clip == bgms[id]) return;
            audioSource.clip = bgms[id];
            audioSource.Play();
        }
    }
}

