using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Roots.UI
{
    public class Dialog : MonoBehaviour
    {
        [SerializeField]
        private bool isOpen = false;
        public bool IsOpen => isOpen;

        [SerializeField]
        private float animationTime = 0.5f;

        private Tweener currentTween = null;

        private void Start()
        {
            transform.localScale = Vector3.zero;
            if (IsOpen)
                currentTween = transform.DOScale(1f, animationTime);
        }

        /// <summary>
        /// 打开/关闭对话框
        /// </summary>
        public void Toggle()
        {
            if (currentTween != null) currentTween.Kill();

            isOpen = !isOpen;
            if (isOpen)
                transform.DOScale(1f, animationTime);
            else transform.DOScale(0f, animationTime);
        }

        public void SetOpen(bool open)
        {
            if (currentTween != null) currentTween.Kill();
            isOpen = open;
            transform.localScale = open ? Vector3.one : Vector3.zero;
        }
    }
}