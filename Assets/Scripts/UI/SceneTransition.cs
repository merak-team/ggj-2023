using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Hotran.UI
{
    public class SceneTransition : MonoBehaviour
    {
        private Image image;

        [Tooltip("场景开始时淡入")]
        [SerializeField]
        private bool transitOnStart = false;

        [SerializeField]
        private float transitionTime = 1f;

        // Start is called before the first frame update
        void Start()
        {
            image = GetComponent<Image>();

            if (transitOnStart) FadeIn();
        }

        public void FadeIn(Action actionOnFaded = null) => DoFade(0f, actionOnFaded);

        public void FadeOut(Action actionOnFaded = null) => DoFade(1f, actionOnFaded);

        private void DoFade(float targetAlpha, Action actionOnFaded) =>
            image.DOFade(targetAlpha, transitionTime).OnComplete(() => actionOnFaded?.Invoke());
    }
}