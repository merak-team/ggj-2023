using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots.UI
{
    public class WaterFlash : MonoBehaviour
    {
        [SerializeField] private float delayRange = 5f;

        private Animator animator;

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            animator.enabled = false;
            Invoke(nameof(StartAnim), Random.Range(0f, delayRange));
        }

        private void StartAnim() => animator.enabled = true;
    }
}