using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots.UI
{
    public class GridCursor : MonoBehaviour
    {
        private Animator animator;
        private SpriteRenderer spriteRenderer;

        private readonly string animKey = "Allowed";
        private int animHash;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            animHash = Animator.StringToHash(animKey);
        }

        public void InitAtRoot(Transform root)
        {
            transform.position = root.position;
            spriteRenderer.color = Color.green;
        }

        public void SetAllowed(bool allowed)
        {
            animator.SetBool(animHash, allowed);
        }

        public void SetLosing(bool isLosing) =>
            spriteRenderer.color = isLosing ? Color.red : Color.white;
    }
}