using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Roots.UI
{
    public class BubbleUi : MonoBehaviour, IPointerClickHandler
    {
        private Animator animator;
        private readonly string openKey = "Open";

        public bool IsOpen => animator.GetBool(openKey);

        void Start()
        {
            animator = GetComponent<Animator>();
            gameObject.SetActive(false);
        }

        public void Open()
        {
            gameObject.SetActive(true);
            animator.SetBool(openKey, true);
        }

        public void Hide() => animator.SetBool(openKey, false);

        public void OnPointerClick(PointerEventData eventData) =>
            Hide();
    }
}