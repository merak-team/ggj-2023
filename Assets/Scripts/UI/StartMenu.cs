using Hotran.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Roots.UI
{
    [System.Obsolete]
    public class StartMenu : MonoBehaviour
    {
        [SerializeField] SceneTransition transition;
        [SerializeField] Button startButton;

        private AsyncOperation loadOperation;

        private void Start()
        {
            loadOperation = SceneManager.LoadSceneAsync(1);
            loadOperation.allowSceneActivation = false;

            startButton.onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            transition.FadeOut(() => loadOperation.allowSceneActivation = true);
        }
    }
}