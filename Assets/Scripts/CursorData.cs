using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    [System.Serializable]
    public struct CursorData
    {
        public Texture2D tex;
        public Vector2 hotspot;
        public void Apply()
        {
            Cursor.SetCursor(tex, hotspot, CursorMode.Auto);
        }
    }
}