using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private float resetPositionX = 13f;

    private Transform _transform;


    private void Awake()
    {
        _transform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime, 0f, 0f);

        if (transform.position.x > resetPositionX)
        {
            Vector3 position = _transform.position;
            position.x = -_transform.position.x;
            _transform.position = position;
        }
    }
}
