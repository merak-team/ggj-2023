using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class RootRuleTile : RuleTile<RootRuleTile.Neighbor> {
    public class Neighbor : TilingRuleOutput.Neighbor
    {
        public const int Sibling = 3;
        public const int NotSibling = 4;
    }

    public override bool RuleMatch(int neighbor, TileBase tile)
    {
        return neighbor switch
        {
            Neighbor.Sibling => tile is RootRuleTile,
            Neighbor.NotSibling => tile is not RootRuleTile,
            _ => base.RuleMatch(neighbor, tile),
        };
    }
}