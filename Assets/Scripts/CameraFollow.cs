using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    public class CameraFollow : MonoBehaviour
    {
        public void SetPosition(Vector3 mouseWorldPos)
        {
            var midPoint = (mouseWorldPos + Camera.main.transform.position) / 2;
            midPoint.x = 0f;
            midPoint.z = 0f;
            transform.position = midPoint;
        }
    }
}