using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    public class Resource : MonoBehaviour
    {
        public int type = 1;
        public int num = 1;
        public bool isStar = false;
        public AudioClip pickSound;
    }
}