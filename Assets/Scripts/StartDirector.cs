using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using DG.Tweening;
using Roots.UI;
using Hotran.Audio;

namespace Roots
{
    /// <summary>
    /// 开始界面到进入游戏的过渡处理
    /// </summary>
    public class StartDirector : MonoBehaviour
    {
        [SerializeField] CinemachineVirtualCamera startCamera;
        [SerializeField] CinemachineVirtualCamera gameCamera;

        [SerializeField] Game game;
        [SerializeField] RootsHandler rootsHandler;

        [Header("UI")]
        [SerializeField] UiHandler ui;
        [SerializeField] Text startTip;
        [SerializeField] CanvasGroup startUI;
        [SerializeField] CanvasGroup gameUI;

        // Start is called before the first frame update
        void Start()
        {
            rootsHandler.FirstRootPlaced.AddListener(() => StartCoroutine(StartGame()));
            ui.GameOver += () => startCamera.Priority = 15;

            gameUI.alpha = 0f;
            gameUI.blocksRaycasts = false;
        }

        private IEnumerator StartGame()
        {
            startTip.GetComponent<Animator>().enabled = false;
            startUI.DOFade(0f, 0.5f);
            yield return new WaitForSeconds(0.2f);

            startCamera.Priority = 0;
            gameUI.DOFade(1f, 0.3f);
            AudioPlayer.Instance.SwitchBGM(0);

            yield return new WaitForSeconds(0.2f);
            gameUI.blocksRaycasts = true;
            game.StartGame();
        }
    }
}